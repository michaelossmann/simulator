from setuptools import setup

with open('requirements.txt') as f:
    requirements = f.read().splitlines()

setup(
   name='rrc_scoring',
   version='1.0',
   description='RRC Scoring Service',
   author='Matt Knight',
   author_email='matt@agitator.tech',
   packages=['rrc_scoring'],  #same as name
   install_requires=requirements,
)

